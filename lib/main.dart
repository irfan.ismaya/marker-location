import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markerlocation/pages/addmarker/add_marker_page.dart';
import 'package:markerlocation/pages/addmarker/location_controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Marker Location',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialBinding: InitialBinding(),
      smartManagement: SmartManagement.keepFactory,
      locale: Locale('id', 'ID'),
      home: AddMarkerPage(),
    );
  }
}

class InitialBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put<LocationController>(LocationController(), permanent: true);
  }
}
