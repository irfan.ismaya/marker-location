class Validator {
  String emptyMessage = 'Please enter field';

  String commonValidator(value, [int minChar = 5]) {
    if (value.isEmpty) {
      return emptyMessage;
    }
    if (value.length < minChar) {
      return 'Minimal $minChar karakter';
    }
    return null;
  }

  String emptyValidator(value) {
    if (value.isEmpty) {
      return emptyMessage;
    }
    if (value.length < 1) {
      return emptyMessage;
    }
    return null;
  }

}