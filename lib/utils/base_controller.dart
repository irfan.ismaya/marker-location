import 'package:get/get.dart';
import 'package:markerlocation/styles/colors.dart';

class BaseController<T> extends GetxController {

  successSnackbar(String error) {
    Get.rawSnackbar(
        message: error,
        backgroundColor: mColorSuccess
    );
  }

  errorSnackbar(String error) {
    Get.rawSnackbar(
        message: error,
        backgroundColor: mColorError
    );
  }

}