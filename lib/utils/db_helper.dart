import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DbProvider {

  static final _databaseVersion = 1;
  static final _databaseName = "marker-location.db";
  static final String _tableLocation = "tb_location";

  DbProvider._privateConstructor();

  static final DbProvider instance = DbProvider._privateConstructor();


  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''create table $_tableLocation ( 
                        _id integer primary key autoincrement, 
                        location text not null,
                        latitude text not null,
                        longitude text not null
          )''');
  }

  Future<int> insertLocation(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(_tableLocation, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRowsLocation() async {
    Database db = await instance.database;
    return await db.query(_tableLocation);
  }

  Future <void> deleteAllRowsLocation() async {
    Database db = await instance.database;
    return await db.delete(_tableLocation);
  }


}