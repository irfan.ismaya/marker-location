import 'package:location/location.dart';

class LocationService {
  Location location = Location();

  static final LocationService _internal = LocationService.internal();

  factory LocationService() {
    return _internal;
  }

  LocationService.internal();

}