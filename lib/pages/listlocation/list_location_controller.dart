import 'package:get/get.dart';
import 'package:markerlocation/models/location_marker.dart';
import 'package:markerlocation/repositories/markerlocation_repository.dart';
import 'package:markerlocation/utils/base_controller.dart';
import 'package:markerlocation/utils/db_helper.dart';

class ListLocationController extends BaseController{
  static ListLocationController get to => Get.find();

  MarkerLocationRepository markerLocationRepository = MarkerLocationRepository.internal(db: DbProvider.instance);
  List<LocationMarker> lists = [];

  @override
  void onInit() {
    initDataLocation();
  }

  void initDataLocation() async{
    lists = await markerLocationRepository.listLocation();
    update();
  }

}