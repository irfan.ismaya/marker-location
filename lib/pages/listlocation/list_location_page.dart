import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markerlocation/models/location_marker.dart';
import 'package:markerlocation/pages/listlocation/list_location_controller.dart';
import 'package:markerlocation/ui/common/app_bar.dart';
import 'package:markerlocation/ui/item/item_location.dart';

class ListLocationPage extends StatelessWidget {

  Widget _buildList(List<LocationMarker> list) {
    if (list.isEmpty) return Center(child: Text('No Data'));

    return ListView.builder(
      shrinkWrap: false,
      itemCount: list.length,
      padding: EdgeInsets.only(bottom: 10),
      itemBuilder: (context, pos) {
        return ItemLocation(
          location: list[pos],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar.of(context).base("List Location", true),
      body: GetBuilder<ListLocationController>(
        init: ListLocationController(),
        builder: (controller){
          return SafeArea(
            child: _buildList(controller.lists),
          );
        }
      ),
    );
  }
}