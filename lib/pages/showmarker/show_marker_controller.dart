import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:markerlocation/models/location_marker.dart';
import 'package:markerlocation/pages/addmarker/location_controller.dart';
import 'package:markerlocation/repositories/markerlocation_repository.dart';
import 'package:markerlocation/utils/base_controller.dart';
import 'package:markerlocation/utils/db_helper.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ShowMarkerController extends BaseController{
  static ShowMarkerController get to => Get.find();
  MarkerLocationRepository markerLocationRepository = MarkerLocationRepository.internal(db: DbProvider.instance);
  Set<Marker> markers = Set();
  List<LocationMarker> lists = [];
  BitmapDescriptor icMarkerIcon;

  GoogleMapController mapController;
  PanelController panelController = PanelController();
  Location location = Location();

  CameraPosition initialPosition = CameraPosition(
    target: LatLng(LocationController.to.latLoc, LocationController.to.lonLoc),
    zoom: 14.4746,
  );

  @override
  // ignore: must_call_super
  void onInit(){ 
    super.onInit();
    loadMarkerIcon(110);
    initDataMarker();
    update();
  }

  onBack() {
    if (panelController.isPanelOpen) {
      panelController.close();
    } else {
      Get.back();
    }
  }

  void loadMarkerIcon(int size) async {
    Uint8List redBus = await getBytesFromAsset('assets/images/icon_marker_bus_red.png', size);
    icMarkerIcon = BitmapDescriptor.fromBytes(redBus);
    update();
  }

  void initDataMarker() async{
    markers = Set();
    lists = await markerLocationRepository.listLocation();

    for(var loc in lists){
      markers.add(
        Marker(
          markerId: MarkerId(loc.location),
          position: LatLng(double.parse(loc.latitude.toString()), double.parse(loc.longitude.toString())),
          icon: icMarkerIcon,
          infoWindow: InfoWindow(title: loc.location.toString())
        ),
      );
    }

    update();  
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
    update();
  }

}