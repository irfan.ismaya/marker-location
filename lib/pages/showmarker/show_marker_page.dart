import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:markerlocation/models/location_marker.dart';
import 'package:markerlocation/pages/showmarker/show_marker_controller.dart';
import 'package:markerlocation/styles/colors.dart';
import 'package:markerlocation/ui/common/app_bar.dart';
import 'package:markerlocation/ui/item/item_location.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ShowMarkerPage extends StatelessWidget {

  final ShowMarkerController controller = ShowMarkerController();

  Widget _buildList(List<LocationMarker> list) {
    if (list.isEmpty) return Center(child: Text('No Data'));

    return ListView.builder(
      shrinkWrap: false,
      itemCount: list.length,
      padding: EdgeInsets.only(bottom: 10),
      itemBuilder: (context, pos) {
        return ItemLocation(
          location: list[pos],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ShowMarkerController>(
        init: controller,
        initState: (_) {
          controller.loadMarkerIcon(110);
          controller.initDataMarker();
        },
        builder: (_){
          return WillPopScope(
            onWillPop: () => controller.onBack(),
            child: Stack(
              children: <Widget>[
                SlidingUpPanel(
                  controller: controller.panelController,
                  defaultPanelState: PanelState.OPEN,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  parallaxEnabled: true,
                  minHeight: 130,
                  maxHeight: 450,
                  panel: Container(
                    decoration: BoxDecoration(
                      color: mColorCardBackground,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child:Padding(padding: const EdgeInsets.all(10),
                      child: _buildList(controller.lists),
                    )
                  ),
                  body: GoogleMap(
                    zoomControlsEnabled: false,
                    mapToolbarEnabled: false,
                    buildingsEnabled: false,
                    trafficEnabled: false,
                    indoorViewEnabled: false,
                    mapType: MapType.normal,
                    initialCameraPosition: controller.initialPosition,
                    markers: controller.markers,
                    onMapCreated: controller.onMapCreated,
                  ),
                ),
                MainAppBar.of(context).floatBackButton(onTap: controller.onBack)
              ],
            ),
          );
        }
      ),
    );
  }
}
