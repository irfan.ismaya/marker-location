import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart' as geo;
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:markerlocation/utils/base_controller.dart';
import 'package:markerlocation/utils/location_service.dart';

class LocationController extends BaseController {
  static LocationController get to => Get.find();

  final geo.Geolocator geoLocator = geo.Geolocator()..forceAndroidLocationManager;

  String myLocation = "";
  double latLoc = 0.0;
  double lonLoc = 0.0;

  Location location = Location();
  LocationService _locationService = LocationService();
  Rx<LocationData> locationData = LocationData.fromMap({
    'latitude': 0.0,
    'longitude': 0.0,
    'accuracy': 0.0,
    'altitude': 0.0,
    'speed': 0.0,
    'speed_accuracy': 0.0,
    'heading': 0.0,
    'time': 0.0
  }).obs;

  CameraPosition initialPosition;

  Future<geo.Placemark> _getPlaceFromPosition(geo.Position position) async {
    try {
      List<geo.Placemark> places = await geoLocator.placemarkFromCoordinates(position.latitude, position.longitude);
      return places[0];
    } catch (e) {
      print(e);
      return null;
    }
  }

  geo.Position _getPositionFromLocation(LocationData location) {
    if (location == null) return null;

    return geo.Position(
        accuracy: location.accuracy,
        altitude: location.altitude,
        heading: location.heading,
        latitude: location.latitude,
        longitude: location.longitude,
        speed: location.speed,
        speedAccuracy: location.speedAccuracy
    );
  }

  void initPlatformState() async {
    await location.changeSettings(accuracy: LocationAccuracy.HIGH, interval: 1000);

    try {
      bool serviceStatus = await location.serviceEnabled();
      print("Location service status: $serviceStatus");

      if (serviceStatus) {
        PermissionStatus status = await location.requestPermission();
        print("Location Permission: $status");

        if (status == PermissionStatus.GRANTED) {
          locationData.bindStream(location.onLocationChanged());
          _locationService.location.onLocationChanged().listen((LocationData result) async {
          geo.Position position = _getPositionFromLocation(result);
          geo.Placemark place = await _getPlaceFromPosition(position);
          myLocation = "Anda Sedang Berada ${place.name} ${place.subLocality} ${place.locality} Kode Pos ${place.postalCode}, \n${place.administrativeArea} ${place.country}";
          latLoc = position.latitude;
          lonLoc = position.longitude;
          initialPosition = CameraPosition(
            target: LatLng(position.latitude, position.longitude),
            zoom: 14.4746,
          );
          update();

          });
        }
      } else {
        bool result = await location.requestService();
        print("Location service status activated after request: $result");
        if (result) initPlatformState();
      }
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  void onInit() async {
    super.onInit();
    initPlatformState();
  }

  @override
  void onClose() async {
    super.onClose();
    try {

    } catch(e) {
      locationData.close();
    }
  }
}