import 'package:flutter/material.dart';
import 'package:flutter_dialogs/flutter_dialogs.dart';
import 'package:get/get.dart';
import 'package:markerlocation/pages/addmarker/add_marker_controller.dart';
import 'package:markerlocation/pages/listlocation/list_location_page.dart';
import 'package:markerlocation/pages/showmarker/show_marker_page.dart';
import 'package:markerlocation/ui/button/button.dart';
import 'package:markerlocation/ui/common/app_bar.dart';
import 'package:markerlocation/ui/form/input_text.dart';
import 'package:markerlocation/utils/validator.dart';

class AddMarkerPage extends StatelessWidget {

  final AddMarkerController controller = Get.put(AddMarkerController());

  Widget _buildAddTripForm(BuildContext context) =>
    Form(
      key: controller.formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
        SizedBox(height: 10,),
        InputText.common(
          controller: controller.locationController,
          isReadOnly: true,
          onTap: () async {
            controller.placePickerLocation(context);
          },
          label: 'Add Location',
          hint: 'Enter Location',
          validator: Validator().commonValidator,
        ),
        SizedBox(height: 10,),
        InputText.common(
          controller: controller.latController,
          label: 'Latitude',
          hint: 'Enter Latitude',
          validator: Validator().commonValidator,
          isLoading: false,
          keyboardType: TextInputType.text,
          isReadOnly: true,
        ),
        SizedBox(height: 10,),
        InputText.common(
          controller: controller.lonController,
          label: 'Longitude',
          hint: 'Enter Longitude',
          validator: Validator().commonValidator,
          isLoading: false,
          keyboardType: TextInputType.text,
          isReadOnly: true,
        ),
        SizedBox(height: 15,),
      ]
    ),
  );

  _showConfirmationAlert({@required BuildContext context, @required Function onTapCancel,@required Function onTapConfirm}) {
    showPlatformDialog(
      context: context,
      builder: (_) => BasicDialogAlert(
        title: Text("Are you sure?"),
        content: Text("Delete all marker location."),
        actions: <Widget>[
          BasicDialogAction(
            title: Text("Cancel"),
            onPressed: onTapCancel,
          ),
          BasicDialogAction(
            title: Text("Delete"),
            onPressed: onTapConfirm,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppBar.of(context).base("Add Marker Location", true),
      body: SingleChildScrollView(
        child: SafeArea(
          child: GetBuilder<AddMarkerController>(
            builder: (_){
              return Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: _buildAddTripForm(context),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: Button(
                              onTap: _.onSubmitAddMarker,
                              text: 'Add Marker'
                            ),
                          ), 
                        ),
                        SizedBox(width: 18,),
                        Expanded(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: Button(
                              onTap: (){
                                _showConfirmationAlert( 
                                  context: context,
                                  onTapCancel: () => Get.back(),
                                  onTapConfirm: (){
                                    _.deleteAllLocation();
                                    Get.back();

                                  }
                                );
                              },
                              text: 'Delete Marker'
                            ),
                          ), 
                        )
                      ]
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: Button(
                              onTap: () => Get.to(ListLocationPage()),
                              text: 'List Marker'
                            ),
                          ), 
                        ),
                        SizedBox(width: 18,),
                        Expanded(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: Button(
                              onTap: () => Get.to(ShowMarkerPage()),
                              text: 'Show Marker'
                            ),
                          ), 
                        )
                      ]
                    ),
                  ],
                ),
              );
            }
          ),
        ),
      )
    );
  }
}
