import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markerlocation/repositories/markerlocation_repository.dart';
import 'package:markerlocation/utils/base_controller.dart';
import 'package:markerlocation/utils/constants.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:markerlocation/utils/db_helper.dart';

class AddMarkerController extends BaseController{
  static AddMarkerController get to => Get.find();

  bool isLoading = false;
  bool autoValidate = false;

  double latFrom = 0.0;
  double lonFrom = 0.0;

  double latTo = 0.0;
  double lonTo = 0.0;

  final formKey = GlobalKey<FormState>();
  LocationResult pickedLocation;
  MarkerLocationRepository markerLocationRepository = MarkerLocationRepository.internal(db: DbProvider.instance);

  TextEditingController locationController = new TextEditingController();
  TextEditingController latController = new TextEditingController();
  TextEditingController lonController = new TextEditingController();
  
  @override
  void onInit() {
    super.onInit();
    clearForm();
  }

  void setLoading(bool loading) {
    isLoading = loading;
    update();
  }

  clearForm() {
    locationController.text = '';
    latController.text = '';
    lonController.text = '';
  }

  void placePickerLocation(BuildContext context) async{
    LocationResult result = await showLocationPicker(
      context, API_KEY,
      initialCenter: LatLng(-6.8981433, 107.6114299),
      automaticallyAnimateToCurrentLocation: true,
      myLocationButtonEnabled: true,
      requiredGPS: true,
      layersButtonEnabled: true,
      countries: ['ID'],
      
      resultCardAlignment: Alignment.bottomCenter,
    );
    latFrom = result.latLng.latitude;
    lonFrom = result.latLng.longitude;
    locationController.text = result.address.toString();
    latController.text = latFrom.toString();
    lonController.text = lonFrom.toString();

    pickedLocation = result;

    update();                  
  }

  void addLocation() async{
    bool statusAdd = await markerLocationRepository.addLocation(
      location: locationController.text, 
      latitude: latController.text.toString(), 
      longitude: lonController.text.toString()
    );

    if(statusAdd){
      successSnackbar('Add data successfully');
    }else{
      errorSnackbar('Add data failed');
    }
  }

  void deleteAllLocation() async{
    bool statusAdd = await markerLocationRepository.addDeleteAll();

    if(statusAdd){
      successSnackbar('Delete data successfully');
    }else{
      errorSnackbar('Add data failed');
    }
  }

  void onSubmitAddMarker() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      if (!isLoading) {
        addLocation();
      }
    } else {
      autoValidate = true;
    }

    update();
  }

}