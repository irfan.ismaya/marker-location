import 'package:flutter/material.dart';
import 'package:markerlocation/models/location_marker.dart';
import 'package:markerlocation/utils/db_helper.dart';

class MarkerLocationRepository{
  static final MarkerLocationRepository _internal = MarkerLocationRepository.internal(db: DbProvider.instance);
  factory MarkerLocationRepository() => _internal;
  MarkerLocationRepository.internal({@required this.db});
  final DbProvider db;

  Future<bool> addLocation({@required String location, @required String latitude,  @required String longitude}) async {
    bool status = false;
    try {
      Map<String, dynamic> row = new Map();
      row['location']     = location;
      row['latitude']     = latitude;
      row['longitude']    = longitude;
      await db.insertLocation(row);
      status = true;
    } catch(e) {
      status = false;
      print('ERROR');
      print(e);
    }

    return status;
  }

  Future<bool> addDeleteAll() async {
    bool status = false;
    try {
      await db.deleteAllRowsLocation();
      status = true;
    } catch(e) {
      status = false;
      print('ERROR');
      print(e);
    }

    return status;
  }

  Future<List<LocationMarker>> listLocation() async {
    var list = await db.queryAllRowsLocation();

    List<LocationMarker> routes = new List<LocationMarker>();
    for(var i = 0; i < list.length; i++) {
      LocationMarker route = LocationMarker(
          list[i]['location'],
          list[i]['latitude'],
          list[i]['longitude']
      );
      routes.add(route);
    }

    return routes;
  }
}