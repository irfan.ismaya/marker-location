class LocationMarker {
  String location;
  String latitude;
  String longitude;

  LocationMarker(this.location, this.latitude, this.longitude);
}