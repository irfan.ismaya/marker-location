import 'package:flutter/material.dart';

// common
const mColorPrimary              = const Color(0xFF00335C);
const mColorSecondary            = const Color(0xFFFAAA20);
const mColorBasedLayout          = const Color(0xFFFAFAFF);
const mColorSurface1             = const Color(0xFFFAFAFF);
const mColorWhite                = const Color(0xFFFFFFFF);
const mColorBlack                = const Color(0xFF000000);
const mColorPlace                = const Color(0xFFF5F3FE);
const mColorGray                 = const Color(0xFFC3BBBB);
const mColorShadow               = const Color.fromRGBO(236, 236, 255, 0.99);
const mColorError                = const Color(0xFFFF0000);
const mColorSuccess              = const Color(0xFF27AE60);

// card
const mColorCardBackground       = const Color(0xFFFFFFFF);

// text
const mTextStandard              = const Color(0xFF0F2A35);
const mTextDark                  = const Color(0xFF032447);
const mTextLight                 = const Color(0xFF8C8C8C);
const mTextHint                  = const Color(0xFFDADADA);
const mTabLabel                  = const Color(0xFF013058);
const mTabUnselectedLabel        = const Color(0xFFCACACA);