import 'package:flutter/material.dart';
import 'colors.dart';

const double radius           = 25.0;
const double appBarRadius     = 40.0;
const double spacingBig       = 15.0;
const double spacingSmall     = 10.0;
const double appBarDefHeight  = 60.0;

const String fontFamily            = 'OpenSans';
const String fontFamilyMono        = 'RobotoMono';
const String placeholderAvatar     = 'assets/images/user_placeholder.png';
const String placeholderNoPicture  = 'assets/images/no_picture.png';

daShape([double radius = 5.0]) => RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(radius)));

ThemeData buildThemeData() {
  return ThemeData(
    fontFamily: fontFamily,
    primaryColor: mColorPrimary,
    accentColor: mColorSecondary,
    textTheme: buildTextTheme(),
    backgroundColor: mColorSurface1,
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      disabledColor: mColorGray,
      buttonColor: mColorSecondary,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(radius))),
    ),
    tabBarTheme: TabBarTheme(
      labelColor: mTabLabel,
      labelStyle: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.normal
      ),
      unselectedLabelColor: mTabUnselectedLabel,
      unselectedLabelStyle: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.normal
      )
    )
  );
}

TextTheme buildTextTheme() {
  return TextTheme(
      headline1: TextStyle(
          fontFamily: fontFamily,
          color: mTextStandard,
          fontSize: 18,
          fontWeight: FontWeight.bold
      ),
      headline2: TextStyle(
          fontFamily: fontFamily,
          color: mTextStandard,
          fontSize: 16,
          fontWeight: FontWeight.w500
      ),
      subtitle1: TextStyle(
        fontFamily: fontFamily,
        color: mTextStandard,
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
      subtitle2: TextStyle(
        fontFamily: fontFamily,
        color: mTextLight,
        fontSize: 12,
        fontWeight: FontWeight.normal,
      ),
      caption: TextStyle(
        fontFamily: fontFamily,
        color: mTextLight,
        fontSize: 10,
      ),
      button: TextStyle(
          fontFamily: fontFamily,
          color: mColorWhite,
          fontWeight: FontWeight.bold,
          fontSize: 14
      )
  );
}

TextStyle buildButtonTextStyle() =>
    TextStyle(
      fontFamily: fontFamily,
      fontWeight: FontWeight.normal,
      letterSpacing: -2,
      fontSize: 18,
    );

TextStyle buildInputTextStyle() =>
    TextStyle(
        color: mColorPrimary,
        fontFamily: fontFamily,
        fontWeight: FontWeight.w200);

TextStyle buildLabelInputTextStyle() => TextStyle(
    fontFamily: fontFamily,
    color: mTextStandard,
    fontWeight: FontWeight.bold,
    fontSize: 14);

BoxShadow buildCommonShadow({ Color shadow = mColorShadow, double radius = 12 }) =>
    BoxShadow(
        spreadRadius: 0,
        color: shadow,
        blurRadius: radius,
        offset: Offset(0, 4));

InputDecoration buildInputDecoration(String label) => InputDecoration(
  errorStyle: TextStyle(
      color: mColorError,
      fontFamily: fontFamily,
      fontWeight: FontWeight.w200
  ),
  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: mTextDark)),
  errorBorder: UnderlineInputBorder(borderSide: BorderSide(color: mColorError)),
  hintText: label,
  hintStyle: TextStyle(
      color: mTextHint
  ),
  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: mTextHint)),
  contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 0.0),
);