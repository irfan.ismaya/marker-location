
import 'package:flutter/material.dart';
import 'package:markerlocation/styles/colors.dart';

class MainAppBar {

  BuildContext context;

  MainAppBar(this.context);

  static MainAppBar of(BuildContext context) => MainAppBar(context);

  base(String text, bool isBack) {
    return AppBar(
      title: Text(text, style: TextStyle(
        fontSize: 18,
        color: Colors.white
      )),
      centerTitle: true,
      elevation: 0,
      automaticallyImplyLeading: isBack,
    );
  }

  floatBackButton({Function onTap}) {
    return SafeArea(
      child: GestureDetector(
        onTap: onTap ?? () => Navigator.pop(context),
        child: Container(
            margin: EdgeInsets.only(left: 16, top: 10),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: mColorCardBackground,
              boxShadow: [BoxShadow(
                  spreadRadius: 0,
                  color: Color(0xFF969696).withOpacity(.16),
                  blurRadius: 3)],
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: Icon(Icons.arrow_back, size: 20, color: Color(0xFF888888),)),
      ),
    );
  }
}