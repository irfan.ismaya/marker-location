import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:markerlocation/styles/colors.dart';


class MarkerLoadingWidget extends StatelessWidget {
  final String text;
  final double size;

  MarkerLoadingWidget({this.text = 'Loading...', this.size = 20});

  Widget _buildLoading() =>
      Platform.isIOS ? CupertinoActivityIndicator()
          : Theme(data: ThemeData(
          primaryColor: mColorPrimary,
          accentColor: Colors.white
      ),
        child: Container(width: size, height: size,
            child: CircularProgressIndicator(backgroundColor: mColorPrimary, strokeWidth: 2)),
      );

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _buildLoading(),
    );
  }
}

class MarkerInkWell extends StatelessWidget {
  final VoidCallback onTap;
  final BorderRadius borderRadius;
  final Widget child;
  final EdgeInsetsGeometry padding;

  MarkerInkWell({@required this.onTap, this.borderRadius = BorderRadius.zero, @required this.child, this.padding = const EdgeInsets.all(0)});

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        color: Colors.transparent,
        elevation: 0.0,
        child: InkWell(
          borderRadius: borderRadius,
          child: Padding(
            padding: padding,
            child: child,
          ),
          onTap: onTap,
        )
    );
  }
}
