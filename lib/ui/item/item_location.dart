import 'package:flutter/material.dart';
import 'package:markerlocation/models/location_marker.dart';

class ItemLocation extends StatelessWidget {
  final LocationMarker location;

  ItemLocation({@required this.location});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Icon(
                    Icons.location_pin, 
                    color: Colors.grey,
                    size: 35,
                  ),
                ),
                Expanded(child: Text(location.location))
              ],
            ),
          ),
          Divider(height: 1,)
        ],
      ),
    );
  }
}