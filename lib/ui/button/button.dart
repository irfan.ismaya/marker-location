import 'package:flutter/material.dart';
import 'package:markerlocation/styles/colors.dart';
import 'package:markerlocation/styles/themes.dart';
import 'package:markerlocation/ui/common/common.dart';

class BaseButton extends StatelessWidget {
  final Widget child;
  final double width;
  final double height;
  final GestureTapCallback onTap;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double radius;
  final Color color;
  final bool noShadow;
  final Color shadowColor;
  final double shadowRadius;

  BaseButton({this.child,
    this.width = double.infinity,
    this.height, this.onTap,
    this.padding,
    this.margin = EdgeInsets.zero,
    this.radius = 5,
    this.color = mColorSecondary,
    this.noShadow = false,
    this.shadowColor = const Color.fromRGBO(245, 194, 0, 0.4),
    this.shadowRadius = 10,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      shadowColor: Colors.grey[50],
      child: Container(
        alignment: AlignmentDirectional.center,
        height: height,
        width: width,
        margin: margin,
        padding: padding,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(radius)),
          color: color,
          boxShadow: [buildCommonShadow(shadow: shadowColor, radius: shadowRadius)]
        ),
        child: MarkerInkWell(
          onTap: onTap,
          borderRadius: BorderRadius.all(Radius.circular(radius)),
          child: Container(
              width: double.infinity,
              child: child),
        ),
      ),
    );
  }
}

class Button extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final bool singleLine;
  final bool isLoading;
  final double letterSpacing;

  Button({@required this.text,
    this.onTap,
    this.padding,
    this.margin,
    this.singleLine = true,
    this.isLoading = false,
    this.letterSpacing = 0});

  @override
  Widget build(BuildContext context) {
    return BaseButton(
      onTap: onTap,
      padding: padding,
      margin: margin,
      child: Padding(
        padding: const EdgeInsets.all(18),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('$text',
                textAlign: TextAlign.center,
                maxLines: singleLine ? 1 : 4,
                overflow: TextOverflow.ellipsis,
                style: buildButtonTextStyle().copyWith(
                    letterSpacing: letterSpacing,
                    color: Colors.white)),
            Visibility(
              visible: this.isLoading,
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: MarkerLoadingWidget(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
