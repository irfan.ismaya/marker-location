
import 'package:flutter/material.dart';
import 'package:markerlocation/styles/colors.dart';
import 'package:markerlocation/styles/themes.dart';

class InputText {

  static Widget common({
    TextEditingController controller,
    String label,
    String hint,
    bool isPassword = false,
    bool isLoading = false,
    bool obscureText = false,
    bool isReadOnly = false,
    FormFieldValidator<String> validator,
    TextInputType keyboardType,
    int maxLines = 1,
    int minLines = 1,
    EdgeInsets padding = const EdgeInsets.only(top: 0, bottom: 15),
    Function onTap
  }) => Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      Text(label, style: buildLabelInputTextStyle(),),
      InputText.base(
        controller: controller,
        label: hint,
        isPassword: isPassword,
        isLoading: isLoading,
        obscureText: obscureText,
        isReadOnly: isReadOnly,
        validator: validator,
        keyboardType: keyboardType,
        maxLines: maxLines,
        minLines: minLines,
        padding: padding,
        onTap: onTap,
      ),
    ],
  );

  static Widget base({
    TextEditingController controller,
    String label,
    bool isPassword = false,
    bool isLoading = false,
    bool obscureText = false,
    bool isReadOnly = false,
    FormFieldValidator<String> validator,
    TextInputType keyboardType,
    int maxLines = 1,
    int minLines = 1,
    EdgeInsets padding = const EdgeInsets.symmetric(
        vertical: 10, horizontal: 0),
    Function onTap
  }) => Padding(
    padding: padding,
    child: TextFormField(
      maxLines: minLines > maxLines ? minLines : maxLines,
      minLines: minLines,
      enabled: !isLoading,
      validator: validator,
      controller: controller,
      keyboardType: keyboardType,
      readOnly: isReadOnly,
      style: buildInputTextStyle().copyWith(
          color: mTextDark,
          fontWeight: FontWeight.w300,
          fontSize: 13
      ),
      onTap: onTap,
      obscureText: isPassword ? obscureText : false,
      textAlignVertical: isPassword ? TextAlignVertical.center : null,
      decoration: InputText.buildTextFieldDecoration(label: label, isPassword: isPassword, obscureText: obscureText, onTap: onTap),
    ),
  );

  static InputDecoration buildTextFieldDecoration({
    String label,
    bool isPassword = false,
    bool obscureText = false,
    Function onTap
  }) {
    InputDecoration base = buildInputDecoration(label);

    return isPassword ? base.copyWith(
      suffixIcon: GestureDetector(
        child: obscureText ? const Icon(Icons.visibility_off)
            : const Icon(Icons.visibility),
        onTap: onTap,
      ),
    ) : base;
  }
}